import fs from 'fs'
import express from 'express'
import path from 'path'

const app = express()

function getFiles(dir) {
    const files = [];
    fs.readdirSync(dir).forEach(file => {
        const absolute = path.join(dir, file);

        if(fs.statSync(absolute).isDirectory())
            return files.concat(getFiles(absolute))


        files.push(absolute)
    })
    return files
}

function base64_encode(file) {
    // read binary data
    var bitmap = fs.readFileSync(file);
    // convert binary data to base64 encoded string
    return new Buffer(bitmap).toString('base64');
}


app.get('/api', (req, res) => {
    try {
        // Get memes
        const memes = getFiles(process.env.MEME_DIR)
        .filter(meme => {  // Filter file extensions
            const extension = meme.substring(meme.length - 4) 
            return extension.toLowerCase() === '.png' || extension.toLowerCase() === '.jpg'
        })
        
        // Pick random meme
        const min = 0
        const max = memes.length - 1
        const luckyMemeIndex = Math.floor(Math.random() * (max - min + 1)) + min
        const luckyMeme = memes[luckyMemeIndex]
        
        //Set header
        res.setHeader('Access-Control-Allow-Origin', '*');
        
        // Convert to Base64
        const response = 'data:image/png;base64,' + base64_encode(luckyMeme)
        res.send(response)
    } catch (err) {
        res.send(err)
    }
})


app.listen(process.env.PORT, () => {
    console.log(`Meme_api running on port: ${process.env.PORT}`)
})
