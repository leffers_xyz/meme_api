FROM node:16-alpine

ARG PORT=3000
ARG MEME_DIR=/opt/meme


#APP
WORKDIR /app

#Copy the necessaries
COPY package.json /app
COPY index.js /app


RUN npm install
RUN npm install express
RUN npm install fs
RUN npm install path


#Run api
EXPOSE $PORT
CMD npm start
